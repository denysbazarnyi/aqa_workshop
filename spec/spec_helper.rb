require 'rspec'
require 'capybara'
require 'capybara/rspec'
require 'site_prism'
require 'selenium-webdriver'

require_relative 'support/feature_helper'
require_relative '../pages/basic_page'

include FeatureHelper

Capybara.app_host = 'http://demo.redmine.org/'

RSpec.configure do |config|
  config.before :all do
    #setting Capybara driver
    Capybara.default_driver = :selenium
    Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1800,1000]))
    end
  end

  config.after :all do
    #setting Capybara driver to reset sessions after all tests are done
    Capybara.reset_sessions!
  end
end
