module FeatureHelper

  def register_user(user_name)
    visit '/account/register'

    find_field('user_login').set user_name
    find_field('user_password').set 'qwerty'
    find_field('user_password_confirmation').set 'qwerty'
    find_field('user_firstname').set 'Test'
    find_field('user_lastname').set 'User'
    find_field('user_mail').set user_name + '@gmail.com'

    find(:xpath, '//*[@id="new_user"]/input[3]').click
    sleep 0.5
  end

  def login_user(user_name)
    visit '/login'
    sleep 0.5
    find_field('username').set user_name
    find_field('password').set 'qwerty'
    find(:xpath, '//*[@id="login-form"]/form/table/tbody/tr[4]/td[2]/input').click
    sleep 0.5
  end

  def logout_user
    find('a.logout').click
    sleep 0.5
  end

end
