feature 'Visitor is able to use authentication', js: true do
  before :all  do
    @user_name = 'test_user_' + Time.now.to_i.to_s
    @basic_page = BasicPage.new
  end

  scenario 'Visitor succesfully get registered' do
    register_user(@user_name)

    expect(@basic_page).to have_content 'Your account has been activated. You can now log in.'
    expect(@basic_page).to have_top_menu
    expect(@basic_page.top_menu).to have_content @user_name
    expect(@basic_page).to have_account
    expect(@basic_page).to have_logout_link

  end

  scenario 'Visitor successfully logs in' do
    login_user(@user_name)

    expect(@basic_page).to have_top_menu
    expect(@basic_page.top_menu).to have_content @user_name
    expect(@basic_page).to have_account
    expect(@basic_page).to have_logout_link
  end

  scenario 'User successfully logs out' do
    login_user(@user_name)

    logout_user

    expect(@basic_page.top_menu).not_to have_content @user_name
    #expect(find(:xpath, '//*[@id="account"]/ul/li[1]/a')).to be_visble
  end
end
