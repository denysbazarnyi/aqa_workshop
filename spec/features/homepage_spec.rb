feature 'Visitor visits homepage', js: true do
  scenario 'Visitor successfully visits homepage' do
    visit '/'
    sleep 1
    expect(page.current_url).to include 'http://demo.redmine.org/'
    expect(page).to have_content 'Redmine demo'
  end

  scenario 'Visitor sucesfully navigates from homepage to login page' do
    visit '/'
    sleep 1
    find(:xpath, '//*[@id="account"]/ul/li[1]/a').click
    sleep 1

    expect(page.current_url).to include '/login'
    expect(find_field('username')).to be_visible
    expect(find_field('password')).to be_visible
    expect(find(:xpath, '//*[@id="login-form"]/form/table/tbody/tr[4]/td[2]/input')).to be_visible
  end

  scenario 'Visitor sucesfully navigates from homepage to register page' do
    visit '/'
    sleep 1
    find(:xpath, '//*[@id="account"]/ul/li[2]/a').click
    sleep 1

    expect(page.current_url).to include '/account/register'

    expect(find_field('user_login')).to be_visible
    expect(find_field('user_password')).to be_visible
    expect(find_field('user_password_confirmation')).to be_visible
    expect(find_field('user_firstname')).to be_visible
    expect(find_field('user_lastname')).to be_visible
    expect(find_field('user_mail')).to be_visible

    expect(find(:xpath, '//*[@id="new_user"]/input[3]')).to be_visible
  end
end
