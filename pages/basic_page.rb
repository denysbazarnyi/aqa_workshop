class BasicPage < SitePrism::Page
  element :top_menu, 'div#top-menu'
  element :account, 'div#account'
  element :logout_link, 'a.logout'
end
